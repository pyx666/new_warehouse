package com.a28.zhangjun.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.a28.zhangjun.myapplication.R;
import com.a28.zhangjun.myapplication.model.exercisefragmentmodel;
import com.google.gson.Gson;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;


public class ExerciseFragment extends Fragment {
    ListView mListview;
    MyAdapter myAdapter;
    public ArrayList<exercisefragmentmodel.Data> lists= new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise, container, false);
        mListview = view.findViewById(R.id.viewpager);
        myAdapter = new MyAdapter();
        mListview.setAdapter(myAdapter);
        RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/exerciselist");
        x.http().post(requestParams, new Callback.CacheCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                exercisefragmentmodel exercisefragmentmodel = gson.fromJson(result, com.a28.zhangjun.myapplication.model.exercisefragmentmodel.class);
                if (exercisefragmentmodel !=null && exercisefragmentmodel.data !=null) {
                    Log.i("ExerciseFragment", result);
                    lists = exercisefragmentmodel.data;
                    myAdapter.notifyDataSetInvalidated();
                }

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Log.i("ExerciseFragment",ex.getMessage());


            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public boolean onCache(String result) {
                return false;
            }
        });
        return view;
    }
    public  class  MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return lists.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            exercisefragmentmodel.Data date = lists.get(position);
            ViewHolder viewHolder;
            if (convertView == null){
                viewHolder = new ViewHolder();
                convertView = View.inflate(getActivity(),R.layout.list_item_layout,null);
                viewHolder.i1Tv = convertView.findViewById(R.id.i1);
                viewHolder.i2Tv = convertView.findViewById(R.id.i2);
                viewHolder.i3Tv = convertView.findViewById(R.id.i3);
                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.i1Tv.setText(position+1+"");
            viewHolder.i2Tv.setText(date.title);
            viewHolder.i3Tv.setText("共计5题");
            return convertView;
        }
        public class ViewHolder{
            public TextView i1Tv;
            public TextView i2Tv;
            public TextView i3Tv;
        }
    }

}
