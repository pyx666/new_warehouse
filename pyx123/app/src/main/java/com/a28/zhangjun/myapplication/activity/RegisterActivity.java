package com.a28.zhangjun.myapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.a28.zhangjun.myapplication.R;
import com.a28.zhangjun.myapplication.model.LoginModel;
import com.google.gson.Gson;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;



public class RegisterActivity extends AppCompatActivity {
    Button mBtn;
    EditText mUsername,mPassword1,mPassword2;
    Button mBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mBack = findViewById(R.id.a1);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mUsername=findViewById(R.id.username);
        mPassword1=findViewById(R.id.password);
        mPassword2=findViewById(R.id.password1);

        mBtn=findViewById(R.id.a9);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = mUsername.getText().toString();
                String passwd = mPassword1.getText().toString();
                String passwd1 = mPassword2.getText().toString();

                if (TextUtils.isEmpty(username)){
                    Toast.makeText(RegisterActivity.this,"请输入用户名",Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(passwd)){
                    Toast.makeText(RegisterActivity.this,"请输入密码",Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(passwd1)){
                    Toast.makeText(RegisterActivity.this,"请再次输入密码",Toast.LENGTH_LONG).show();
                    return;
                }
                if(!passwd.equals(passwd1)){
                    Toast.makeText(RegisterActivity.this,"两次密码不一致",Toast.LENGTH_LONG).show();
                    return;
                }
                RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/register");

                requestParams.addBodyParameter("username",username);
                requestParams.addBodyParameter("password",passwd);

                x.http().post(requestParams, new Callback.CacheCallback<String>() {

                    @Override
                    public void onSuccess(String result) {
                        Gson gson = new Gson();
                       LoginModel model =gson.fromJson(result,LoginModel.class);
                        if (model.code == 0){
                            Toast.makeText(RegisterActivity.this,"注册成功",Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(RegisterActivity.this,"注册失败",Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {
                        Log.i("RegisterActivity","fail:"+ex.getMessage());

                    }

                    @Override
                    public void onCancelled(CancelledException cex) {

                    }

                    @Override
                    public void onFinished() {

                    }

                    @Override
                    public boolean onCache(String result) {
                        return false;
                    }
                });
            }
        });

    }
}
