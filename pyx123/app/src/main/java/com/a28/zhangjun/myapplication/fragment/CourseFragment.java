package com.a28.zhangjun.myapplication.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a28.zhangjun.myapplication.R;
import com.a28.zhangjun.myapplication.model.BannerModel;
import com.google.gson.Gson;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;


public class CourseFragment extends Fragment {
    ViewPager mViewpager;
    MyViewPager myViewPager;
    LinearLayout mDotLayout;

    RecyclerView mRecycleview;
    RecycleAdapter myAdapter;
    public ArrayList<BannerModel.Data> bannerlist = new ArrayList<>();
            ArrayList<ImageView> imageViews = new ArrayList<>();
    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.i("CourseFragment","hand message");


            handler.sendEmptyMessageDelayed(1,2000);
            mViewpager.setCurrentItem(mViewpager.getCurrentItem()+1);
        }

    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_course, container, false);
        mViewpager = view.findViewById(R.id.viewpager);
        myViewPager = new MyViewPager();
        mDotLayout = view.findViewById(R.id.dotlayout);
        mRecycleview = view.findViewById(R.id.recycleview);

        myAdapter = new RecycleAdapter();
        mRecycleview.setAdapter(myAdapter);
        mViewpager.setAdapter(myViewPager);
        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i==0){
                    mViewpager.setCurrentItem(bannerlist.size()-2,false);
                }
                if (i==bannerlist.size()-1){
                    mViewpager.setCurrentItem(1,false);
                }
                setDots();
                Log.i("CrouseFragment","CrouseFragment:" +i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/banner");
        x.http().post(requestParams, new Callback.CacheCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                BannerModel bannerModel = gson.fromJson(result, BannerModel.class);
                if(bannerModel != null && bannerModel.data != null ){
                    bannerlist.addAll(bannerModel.data);
                    bannerlist.add(bannerModel.data.get(0));
                    bannerlist.add(0,bannerModel.data.get(bannerModel.data.size()-1));
                    mViewpager.setAdapter(myViewPager);
                    mViewpager.setCurrentItem(1);
                    Log.i("CourseFragment","开始发送");
                    handler.sendEmptyMessageDelayed(1,2000);
                    for (int i = 0; i < bannerModel.data.size(); i++ ){
                        ImageView imageView = new ImageView(getActivity());
                        imageView.setBackgroundResource(R.drawable.courseimg);
                        LinearLayout.LayoutParams params  =  new LinearLayout.LayoutParams(30,30);
                        params.leftMargin = 10;
                        params.rightMargin = 10;
                        imageView.setLayoutParams(params);

                        mDotLayout.addView(imageView);
                        imageViews.add(imageView);
                    }
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public boolean onCache(String result) {
                return false;
            }
        });
        return view;
    }

    public void setDots(){
        for (int i = 0; i < imageViews.size(); i++ ){
            if(i == mViewpager.getCurrentItem()-1){
                imageViews.get(i).setSelected(true);
            }
            else {
                imageViews.get(i).setSelected(false);
            }
        }
    }

    public class RecycleAdapter extends RecyclerView.Adapter<MyViewHoder>{

        @NonNull
        @Override
        public MyViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = View.inflate(getActivity(),R.layout.list_item_layout,null);
            MyViewHoder myViewHoder = new MyViewHoder(view);
            return myViewHoder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHoder myViewHoder, int i) {
            myViewHoder.titleTv.setText(i+1+"");
        }

        @Override
        public int getItemCount() {
            return 10;
        }
    }
        public class MyViewHoder extends  RecyclerView.ViewHolder{
            public TextView titleTv;

            public MyViewHoder(@NonNull View itemView) {
                super(itemView);
                titleTv = itemView.findViewById(R.id.t1);
            }
        }
    public class MyViewPager extends PagerAdapter{

        @Override
        public int getCount() {
            return bannerlist.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            ImageView imageView = new  ImageView(getActivity());
            x.image().bind(imageView,bannerlist.get(position).bannerimg);
            container.addView(imageView);
            return imageView;
        }
    }

}
