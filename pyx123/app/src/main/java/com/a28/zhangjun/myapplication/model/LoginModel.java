package com.a28.zhangjun.myapplication.model;

import android.provider.ContactsContract;

public class LoginModel {
    public String msg;
    public int code;
    public Data data;

    public class Data{
        public int expire;
        public String token;
    }
}
