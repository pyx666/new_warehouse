package com.a28.zhangjun.myapplication.model;

import java.util.ArrayList;

public class BannerModel {
    public String msg;
    public int code;
    public ArrayList<Data>data;

    public class Data{
        public int id;
        public String title;
        public String bannerimg;
    }
}