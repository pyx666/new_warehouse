package com.a28.zhangjun.myapplication.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.a28.zhangjun.myapplication.R;
import com.a28.zhangjun.myapplication.fragment.CourseFragment;
import com.a28.zhangjun.myapplication.fragment.ExerciseFragment;
import com.a28.zhangjun.myapplication.fragment.MyFragment;

public class MainActivity1 extends AppCompatActivity {
    RelativeLayout mCouseBtn,mExerciseBtn,myBtn;
    Fragment coursefragment = new CourseFragment();
    Fragment exerfragment = new ExerciseFragment();
    Fragment myfragment = new MyFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        getSupportFragmentManager().beginTransaction().replace(R.id.layout,coursefragment).commit();
        mCouseBtn = findViewById(R.id.w1);
        mCouseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,coursefragment).commit();
                mCouseBtn.setSelected(true);
                mExerciseBtn.setSelected(false);
                myBtn.setSelected(false);
            }
        });
        mExerciseBtn = findViewById(R.id.w2);
        mExerciseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,exerfragment).commit();
                mCouseBtn.setSelected(false);
                mExerciseBtn.setSelected(true);
                myBtn.setSelected(false);
            }
        });
        myBtn = findViewById(R.id.w3);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,myfragment).commit();
                mCouseBtn.setSelected(false);
                mExerciseBtn.setSelected(false);
                myBtn.setSelected(true);
            }
        });
        mCouseBtn.setSelected(true);
        mExerciseBtn.setSelected(false);
        myBtn.setSelected(false);
    }
}
