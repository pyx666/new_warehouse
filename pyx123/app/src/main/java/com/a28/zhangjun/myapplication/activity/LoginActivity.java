package com.a28.zhangjun.myapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import com.a28.zhangjun.myapplication.R;
import com.a28.zhangjun.myapplication.model.LoginModel;
import com.google.gson.Gson;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

public class LoginActivity extends AppCompatActivity {
    Button mBtn;
    EditText mUserName,mPasswd;
    TextView mRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRegister = findViewById(R.id.b3);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(inent);
            }
        });
        mUserName = findViewById(R.id.username);
        mPasswd = findViewById(R.id.password);
        mBtn = findViewById(R.id.a9);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/login");
                String uesrname = mUserName.getText().toString();
                String passwd = mPasswd.getText().toString();
                requestParams.addBodyParameter("username",uesrname);
                requestParams.addBodyParameter("password",passwd);
                x.http().post(requestParams, new Callback.CacheCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        Gson gson = new Gson();
                        LoginModel model = gson.fromJson(result,LoginModel.class);
                        Log.i("LoginActivity","success:"+result);
                        if (model.code == 0){
                            Toast.makeText(LoginActivity.this,"登陆成功",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(LoginActivity.this,"用户名或密码错误",Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {
                        Log.i("LoginActivity","fail:"+ex.getMessage());
                    }

                    @Override
                    public void onCancelled(CancelledException cex) {

                    }

                    @Override
                    public void onFinished() {

                    }

                    @Override
                    public boolean onCache(String result) {
                        return false;
                    }
                });
            }
        });
    }


}
