package com.a28.zhangjun.myapplication.model;

import java.util.ArrayList;

public class exercisefragmentmodel {
    public String msg;
    public int code;
    public ArrayList<Data>data;

    public class Data{
        public int id;
        public String title;
        public String des;
        public String time;
        public String img;
        public String subtitle;
    }
}

