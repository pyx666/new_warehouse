package com.a28.zhangjun.myapplication.activity;

import android.app.Application;

import org.xutils.x;

public class MyApplication extends Application {
    public void onCreate(){
        x.Ext.init(this);
        super.onCreate();
    }
}
