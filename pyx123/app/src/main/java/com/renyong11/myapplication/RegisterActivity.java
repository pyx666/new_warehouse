package com.renyong11.myapplication;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.renyong11.myapplication.Model.aaa;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;
public class RegisterActivity extends AppCompatActivity {
    EditText mUserName,mPassWord;
    Button mBtn;
    ImageView Btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
         mBtn=findViewById(R.id.register);
         mUserName=findViewById(R.id.username);
         mPassWord=findViewById(R.id.password);
         mPassWord=findViewById(R.id.password2);
         Btn=findViewById(R.id.back);
         mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/register");
                String username = mUserName.getText().toString();
                if (TextUtils.isEmpty(username)){
                    Toast.makeText(RegisterActivity.this,"用户名不能为空",Toast.LENGTH_LONG).show();
                  return;
                }
                String password = mPassWord.getText().toString();
                if (TextUtils.isEmpty(password)){
                    Toast.makeText(RegisterActivity.this,"密码不能为空",Toast.LENGTH_LONG).show();
                    return;
                }
                String password2 = mPassWord.getText().toString();
                if (TextUtils.isEmpty(password2)){
                    Toast.makeText(RegisterActivity.this,",密码不能为空",Toast.LENGTH_LONG).show();
                    return;
                }
                if(!password.equals(password2)){
                        Toast.makeText(RegisterActivity.this,",两次密码输入不一致",Toast.LENGTH_LONG).show();
                        return;
                    }
                requestParams.addBodyParameter("username",username);
                requestParams.addBodyParameter("password",password);
                x.http().post(requestParams, new Callback.CacheCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        Gson gson = new Gson();
                        aaa model = gson.fromJson(result,aaa.class);
                        Log.i("RegisterActivity","success:"+result);
                        if(model.code==0) {
                            Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {
                        Log.i("RegisterActivity","fail:"+ex.getMessage());
                        Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onCancelled(CancelledException cex) {

                    }
                    @Override
                    public void onFinished() {

                    }

                    @Override
                    public boolean onCache(String result) {
                        return false;
                    }
                });
            }
        });
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }

}
